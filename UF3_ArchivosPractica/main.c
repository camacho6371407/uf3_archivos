#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define BB while(getchar()!='\n')
#define MSS_ERROR_SELECCION "Elije una opcion del 1 - 3 !!!"
#define MSS_ESCRITURA "\n  *** *** ESCRITURA *** ***  \n"
#define MSS_LECTURA "\n  *** *** LECTURA *** ***  \n"
#define MSS_CONFIRMACION "Seguro que quirers eliminar este registro? (s/n): "
#define MSS_ELIMINADO "\n  *** *** ELIMINADO *** ***  \n"


#define MSS_HTML " *** *** HTML GENERADO *** *** \n"
#define MAX_HTML 10000

#define MAX_NOM 35

struct Cantantes{
    char nom[MAX_NOM+1];
    int nAlbumes;
    bool estaVivo;
    char marcaBorrar;
    int codigo;
};

void imprimeMenu();
void recibeOpcion(int *opc);
void imprimeError(char mss[]);
void preguntaContinuar(bool *seguir);
int alta(FILE * fitxer, struct Cantantes *p);
void entradaCantante(struct Cantantes *p);
int ecriureCantanteDisc(FILE *fitxer,struct Cantantes p, int nReg);
int llegirCantanteDisc(FILE *fitxer,struct Cantantes *p, int nReg);
void imprimirCantante(struct Cantantes p);
int consulta(FILE *fitxer);
int generaHtml(FILE *fitxerHtml, FILE *fitxer);
void generaColHtml(struct Cantantes *p, char cadenaAux[]);
void iniciaFitxer(FILE *fitxer);
void vaciarCadena(char cadena[], int max);
void limpiarPantalla();
void dormir();
void consultaUnica(FILE *fitxer);
void preguntarID(int *id);
void buscarPorID(FILE *fitxer, int idTpm, struct Cantantes *p);
void borrarRegistro(FILE *fitxer, int idTmp);
void eliminar(FILE *fitxer);



int main()
{
    bool cont;
    int opcion;
    struct Cantantes p;
    FILE *fitxer=NULL;
    FILE *fitxerHtml=NULL;

    iniciaFitxer(fitxer);   //Se abre el archivo en modo 'w' para que se elimine por si se creó en ejecuciones pasadas
                            //En el resto de funciones se ejecuta el archivo en
                            //modo 'a' para siempre tratar con el mismo archivo
    opcion = 0;
    while(opcion != 9){
        cont = true;
        imprimeMenu();
        recibeOpcion(&opcion);
        if(opcion > 5  && opcion != 9){
            limpiarPantalla();
            imprimeError(MSS_ERROR_SELECCION);
        }else{
            switch(opcion){
                case 1:
                    limpiarPantalla();
                    printf("%s",MSS_ESCRITURA);
                    do{                             //Se hará un registro a la vez
                        alta(fitxer, &p);           //y se prgunta si se desea continuar
                        preguntaContinuar(&cont);
                    }while(cont);
                    limpiarPantalla();
                    break;
                case 2:
                    limpiarPantalla();
                    printf("%s",MSS_LECTURA);
                    consulta(fitxer);
                    dormir();
                    break;
                case 3:
                    limpiarPantalla();
                    printf("%s", MSS_HTML);
                    generaHtml(fitxerHtml, fitxer);
                    dormir();
                    break;
                case 4:
                    limpiarPantalla();
                    consultaUnica(fitxer);
                    dormir();
                    break;
                case 5:
                    eliminar(fitxer);
                    dormir();
            }
        }
    }


    return 0;
}

void eliminar(FILE *fitxer){
    int id;
    char opc;
    struct Cantantes p;
    preguntarID(&id);
    buscarPorID(fitxer, id, &p);
    imprimirCantante(p);
    printf("%s", MSS_CONFIRMACION);
    scanf("%c", &opc);BB;
    if(opc == 's'){
        borrarRegistro(fitxer, id);
        printf("%s", MSS_ELIMINADO);
    }
}

void borrarRegistro(FILE *fitxer, int idTmp){
    int n;
    struct Cantantes p;
    fitxer= fopen("cantantes.bin","rb+");
    if( fitxer == NULL ) {
        printf("Error en obrir el fitxer ");

    }
    while(!feof(fitxer)){
        n=fread(&p,sizeof(struct Cantantes),1,fitxer);
        if(!feof(fitxer)){
            if(n==0) {
                printf("Error de lectura");

            }else{
                if(p.marcaBorrar !='*' && p.codigo == idTmp ){
                    // Una para atras
                    if( fseek( fitxer, -(long) sizeof(struct Cantantes), SEEK_CUR ) ) printf("Error al hechar atras");
                    //sortir del while(!eof(f)) (break)

                    p.marcaBorrar='*';

                    n=fwrite(&p, sizeof(struct Cantantes), 1 , fitxer);
                    //control d’error d’escriptura
                    if(n!=1){
                        printf("\nBaixa: Error d'escriptura");

                    }

                    break;
                }
            }
        }
    }
    fclose(fitxer);
}

void consultaUnica(FILE *fitxer){
    int id;
    struct Cantantes p;
    preguntarID(&id);
    buscarPorID(fitxer, id, &p);
    imprimirCantante(p);
}

void preguntarID(int *id){
    printf("ID del cantante a buscar: ");
    scanf("%i", id);BB;
}

void buscarPorID(FILE *fitxer, int idTpm, struct Cantantes *p){
    int n;
    fitxer= fopen("cantantes.bin","rb");
    if( fitxer == NULL ) {
        printf("Error en obrir el fitxer ");
    }
    while(!feof(fitxer)){
        n=fread(p,sizeof(struct Cantantes),1,fitxer);
        if(!feof(fitxer)){
            if(n==0) {
                printf("Error de lectura");
            }else{
                if(p->marcaBorrar !='*' && p->codigo == idTpm ){
                    // Una para atras
                    if( fseek( fitxer, -(long) sizeof(struct Cantantes), SEEK_CUR ) ) printf("Error al hechar atras");
                    //sortir del while(!eof(f)) (break)
                    break;
                }
            }
        }
    }
    fclose(fitxer);
}




void iniciaFitxer(FILE *fitxer){
    fitxer= fopen("cantantes.bin","wb");
    fclose(fitxer);
}

void preguntaContinuar(bool *seguir){
    char opcion;
    printf("Ingresar mas altas? (s/n):  ");
    scanf("%c", &opcion);BB;
    if(opcion == 'n') *seguir=false;
}

void limpiarPantalla(){
    //system("clear");                //Para linux
    system("cls");                //Para windows
}

void dormir(){
    printf("\n\n\nOprime ENTER para continuar...");
    char c;
    scanf("%c",&c);
    limpiarPantalla();
}

void imprimeMenu(){
    printf(" *****   MENU   ***** \n");
    printf("    1. Alta\n");
    printf("    2. Listar por pantalla\n");
    printf("    3. Listar en pagina web\n");
    printf("    4. Consulta\n");
    printf("    5. Eliminar registro\n\n");
    printf("    9. Salir\n\n");
    printf("    Elige la opcion 1-3: ");
}

void recibeOpcion(int *opc){
    scanf("%d", opc);BB;
}

void imprimeError(char mss[]){
    printf("   ---   ---   ---   ---   ---   \n");
    printf("*  %s  *\n", mss);
    printf("   ---   ---   ---   ---   ---   \n");
}

void imprimirCantante(struct Cantantes p){
    if(p.marcaBorrar != '*'){
        printf(" *-*-Cantante:\n");
        printf("Nom: %s\n",p.nom);
        printf("Edat: %i\n", p.nAlbumes);
        if(p.estaVivo) printf("viu?: Si\n");
        else printf("viu?: No\n");
        printf("ID: %d\n", p.codigo);
    }
}


int alta(FILE *fitxer, struct Cantantes *p){
    int error = 0;

    //obrir fitxer escriptura
    fitxer= fopen("cantantes.bin","ab");
    if(fitxer==NULL){
        error=1; //error obrir fitxer
    }else{
        entradaCantante(p);
        ecriureCantanteDisc(fitxer, *p, 1);
    }
    if(fclose(fitxer)){
        printf("error al tancar el fitxer...");
    }

    return error;
}

void entradaCantante(struct Cantantes *p){
    char estViu;
    printf("Nom: ");
    scanf("%35[^\n]",p->nom);BB;// (*p).nom
    printf("Numero de albumes: ");
    scanf("%i",&p->nAlbumes);BB;// &(*p).edat
    printf("Esta vivo?: s/n ");
    scanf("%c", &estViu);BB;// (*p).poblacio
    if(estViu=='s') p->estaVivo = true;
    else p->estaVivo = false;
    p->marcaBorrar = '\0';
    printf("Ingresa el ID: ");
    scanf("%i", &p->codigo);BB; //Identificador para diferenciar cada cantante
}

int ecriureCantanteDisc(FILE *fitxer,struct Cantantes p, int nReg){
    int n,error=0;
    n=fwrite(&p,sizeof(struct Cantantes),nReg,fitxer);
    if(n!=nReg){
        error=2; //error numero registres escrits
    }
    return error;
}

int consulta(FILE *fitxer){
    int error = 0, n;
    struct Cantantes p;

    fitxer= fopen("cantantes.bin","rb");//fitxer binari (b) d'afegir (a)
    if(fitxer==NULL){
        error=1; //error obrir fitxer
    }
    while(!feof(fitxer)){
        n=llegirCantanteDisc(fitxer, &p, 1);
        if(n==0){
            imprimirCantante(p);
        }
        if(n==1){
            printf("\nError en obrir fitxer...\n");break;
        }
        if(n==3){
            printf("\nError de lectura...\n");break;
        }
    }
    if(fclose(fitxer)){
        printf("error al tancar el fitxer...");
    }

    return error;
}

int llegirCantanteDisc(FILE *fitxer,struct Cantantes *p, int nReg){
    int n,error=0;
    //escriure les dades de la persona al fitxer persones.bin
    n=fread(p,sizeof(struct Cantantes),nReg,fitxer);
    if(!feof(fitxer)){
        if(n==0){
            error=3; //error de lectura
        }
    }else{
        error=4 ; //És un avís. Ha arribat al final del fitxer.
    }
    return error;
}

int generaHtml(FILE *fitxerHtml, FILE *fitxer){

    struct Cantantes p;
    int error=0,n;
    char cadenaTmp[100];
    char cap[]="<!DOCTYPE html> \
            <html lang=\"en\"> \
            <head> \
                <meta charset=\"UTF-8\"> \
                <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"> \
                <title>Document</title> \
            </head> \
            <body> \
            <table><tr><th>Nombre</th><th>NumeroAlbumes</th><th>Vivo?</th></tr>";

    char cos[] = " ";

    char peu[]="</body>\
                </html>";

    fitxer= fopen("cantantes.bin","rb");//fitxer binari (b) d'afegir (a)
    if(fitxer==NULL){
        error=1; //error obrir fitxer
    }else{
        fitxerHtml = fopen("index.html", "w");
        if(fitxerHtml==NULL){
            error = 1;
        }else{
            error = fputs(cap, fitxerHtml);
            if(error == EOF){
                error = 2;
            }
             while(!feof(fitxer)){
                n=llegirCantanteDisc(fitxer, &p, 1);
                if(n==0 && p.marcaBorrar != '*'){
                    vaciarCadena(cadenaTmp, 100);
                    generaColHtml(&p, cadenaTmp);
                    strcat(cos, cadenaTmp);
                }
                if(n==1){
                    printf("\nError en obrir fitxer...\n");break;
                }
                if(n==3){
                    printf("\nError de lectura...\n");break;
                }
            }

            error = fputs(cos, fitxerHtml);
            if(error == EOF){
                error = 2;
            }

            error = fputs(peu, fitxerHtml);
            if(error == EOF){
                error = 2;
            }


            if(fclose(fitxerHtml)){
                error = 3;
            }

            if(fclose(fitxer)){
                error = 3;
                }
        }
    }

    return error;
}

void generaColHtml(struct Cantantes *p, char cadenaAux[]){
    char aux[100];

    strcat(cadenaAux, "<tr><td>");
    strcat(cadenaAux, p->nom);
    strcat(cadenaAux, "</td><td>");
    sprintf(aux, "%d", p->nAlbumes); //Convertir numero a cadena
    strcat(cadenaAux, aux);
    strcat(cadenaAux, "</td><td>");
    if(p->estaVivo){
        strcat(cadenaAux, "Esta vivo");
    }else{
        strcat(cadenaAux, "NO vive");
    }
    strcat(cadenaAux, "</td></tr>");

}

void vaciarCadena(char cadena[], int max){
    int indx = 0;
    while(indx<max){
        cadena[indx]='\0';
        indx++;
    }
}





