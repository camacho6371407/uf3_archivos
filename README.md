# UF3 - PRACTICA 

La practirca consta de crear un menu que nos permitiera realizar acciones como consultarl y eliminar dator que previamente se ingresaron.  

>La últimas dos opcines son adcionales y hacen al programa un poco más interesante.  

## Ejemplo de ejecución :star:

![ejecucion](ejemplo_ejecucion.png)
  
## Explicacion general del funcionamiento  
  
El programa funciona con una iteracion principal que muestra por pantalla el menu de opciones con lo que se puede hacer, y no deja avanzar si no elijes un valor acertado. 

![menu](iteracion.png)

Seguido hay un switch case que dependiento lo que elijas, ejecutará las acciones correspondientes para poder realizar la tarea asignada. 

![switch](opciones.png)

>Puede que ver tantas acciones dentro de cada caso haga un poco difícil la lectura y fácil comprensión del código. Pero cada uno de ellos tiene un función que realiza lo que dice su enunciado en el menu. 

Las funciones / acciones: **alta, consulta, generaHtml, consultaUnica y eliminar** son el nivel más alto del código que realiza su correspondiente enunciado. El resto son para hacer un poco más estético o interactivo el programa. Tales como lo son las acciones:  

+ **limpiarPantalla( )**: Tal y como lo dice su nombre, limpia la salida por pantalla eliminando lo que había el texto resto de la ejecucion. :exclamation: **Esta es la única función que hay que modificar dependiendo del SO desde donde se ejecuta el programa**:exclamation:
![limpiar](limpiaPantalla.png)  
Es un cambio simple que no interfiere con el funcionamiento del programa  


* **dormir( ):** Detiene la ejecucion del programa con un scanf() que espera que se oprima ENTER para continuar con la ejecucion.:zzz::zzz:  
![dormir](dormir.png) 